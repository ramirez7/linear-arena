```
git config --global url."git://github.com/ghc/packages-".insteadOf     git://github.com/ghc/packages/;
git config --global url."http://github.com/ghc/packages-".insteadOf    http://github.com/ghc/packages/;
git config --global url."https://github.com/ghc/packages-".insteadOf   https://github.com/ghc/packages/;
git config --global url."ssh://git\@github.com/ghc/packages-".insteadOf ssh://git\@github.com/ghc/packages/;
git config --global url."git\@github.com:/ghc/packages-".insteadOf      git\@github.com:/ghc/packages/;
```

```
linear-ghc --interactive -i/home/armando/work/linear-base/src/
```

An Arena in..
* [C](https://gitlab.haskell.org/ghc/ghc/blob/master/rts/Arena.c)
