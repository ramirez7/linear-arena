{-
TODOs
- Use the nixpkgs fork with the most recent rev
  - https://github.com/facundominguez/nixpkgs/commit/374c9daafa00c8a6624e7ad1052ac99e7ad9efe6
  - linear-ghc remote already added
- Bring linear-base into nixpkgs
- Make this a cabal project!
---
-- POC: Command line program that runs in a loop w/ the arena
-- Doesn't matter what - just allocate some stuff, use the stuff, free the stuff, reset + inc the gen
-- + (polymorphically) recur
---
- Use ST trick to uniquely tag the Arena so you can't use some OTHER  Arena to access this Arena's stuff
- An arena (linear) monad to avoid munging arenas?

Things still needed
- Multiplicity annotations/inference for usable lambdas. Right now, it isn't pretty.
-}

{-# LANGUAGE LinearTypes #-}
{-# LANGUAGE MagicHash #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}

import Control.Exception (bracket)
import qualified Foreign.Marshal.Alloc
import Foreign.Ptr
import Foreign.Storable (Storable)
import Data.Functor.Identity
import qualified Unsafe.Coerce
import GHC.Exts (TYPE, RuntimeRep, Int(..))
import qualified System.IO.Unsafe


-- | This is basically 'Void' at the type level.
--
-- In practice, we use the ST trick to get a type variable of this kind
-- and then when we 'reset' an 'Arena tag', the new Arena is tagged with 'Reset tag'
-- meaning it cannot be used with 'ArenaPtr's of the previous tag anymore.
data Tag = Reset Tag

-- | Just a region of memory and a current offset
--
-- Allocation = pointer bump
-- Reset = Set offset to 0
data Arena (tag :: Tag) = Arena
  { memory :: (Ptr ())
  , offset :: !Int
  }

-- | A Ptr owned by the Arena identified by tag
newtype ArenaPtr (tag :: Tag) a = ArenaPtr () -- TODO

-- Should this be 'Unrestricted r'?
withArena :: Int #-> (forall tag. Arena tag #-> r) #-> r
withArena = toLinear2 unsafeWithArena
  where
    -- We move the tag outside here because we're cheating anyways ;)
    unsafeWithArena :: forall tag r. Int -> (Arena tag #-> r) -> r
    unsafeWithArena size f = System.IO.Unsafe.unsafeDupablePerformIO $
      bracket (Foreign.Marshal.Alloc.mallocBytes size) Foreign.Marshal.Alloc.free $ \ptr -> do
        let arena = Arena ptr 0
        Prelude.pure $ f arena

-- | A no-op, but tells the type system you are done using an Arena
done :: Arena tag #-> Unrestricted ()
done = undefined


-- | Allocate a new pointer in the arena for the current generation
alloc :: Storable a
      => Arena tag
      #-> (Arena tag, ArenaPtr tag a)
alloc = undefined


free ::  Storable a
     =>  ArenaPtr tag a -- do we need to pass the arena too?
     #-> ()
free = undefined


-- | Takes the Arena for invariant enforcement, but does not do
-- anything w/the Arena itself
poke      ::  Storable a 
          =>  Arena tag
          #-> ArenaPtr tag a
          #-> a
          #-> (Arena tag, ArenaPtr tag a)
poke = undefined


-- | Takes the Arena for invariant enforcement, but does not do
-- anything w/the Arena itself
peek      ::  Storable a 
          =>  Arena tag
          #-> ArenaPtr tag a
          #-> (Arena tag, ArenaPtr tag a, Unrestricted a)
peek = undefined


-- | Reset the arena, 'Reset'ing its tag. This ensures
-- previously-allocated pointers can no longer be peeked or poked
reset ::  Arena tag
      #-> Arena (Reset tag)
reset (Arena memory offset) = consume offset `lseq` Arena memory 0


--
--
--
--------------------------------------------------------------------------------------------------
-- tests --

{-
test2 :: Int -> ()
test2 size = withArena 1 f
  where
    f :: forall tag. Arena tag #-> ()
    f a = runIdentity' $ do
      (a', ptr) <- Identity $ alloc @Int a
      Identity $ free ptr `lseq` done a'

test :: Arena tag #-> Arena tag #-> ()
test = undefined

linear :: (a #-> b) -> (a #-> b)
linear = Prelude.id
-}

--
--
--
--------------------------------------------------------------------------------------------------
-- linear-base copy-pasta --

-- | @Unrestricted a@ represents unrestricted values of type @a@ in a linear context,
data Unrestricted a where
  Unrestricted :: a -> Unrestricted a

-- | Project an @a@ out of an @Unrestricted a@. If the @Unrestricted a@ is
-- linear, then we get only a linear value out.
unUnrestricted :: Unrestricted a #-> a
unUnrestricted (Unrestricted a) = a

-- | Linearly typed @unsafeCoerce@
coerce :: a #-> b
coerce = Unsafe.Coerce.unsafeCoerce Unsafe.Coerce.unsafeCoerce

-- | Converts an unrestricted function into a linear function
toLinear
  :: forall (r1 :: RuntimeRep) (r2 :: RuntimeRep)
     (a :: TYPE r1) (b :: TYPE r2).
     (a -> b) #-> (a #-> b)
toLinear = coerce

-- | Like 'toLinear' but for two-argument functions
toLinear2
  :: forall (r1 :: RuntimeRep) (r2 :: RuntimeRep) (r3 :: RuntimeRep)
     (a :: TYPE r1) (b :: TYPE r2) (c :: TYPE r3).
     (a -> b -> c) #-> (a #-> b #-> c)
toLinear2 = coerce

-- | Like 'toLinear' but for three-argument functions
toLinear3 :: (a -> b -> c -> d) #-> (a #-> b #-> c #-> d)
toLinear3 = coerce


class Consumable a where
  consume :: a #-> ()

instance Consumable () where consume () = ()

instance Consumable Int where
  -- /!\ 'Int#' is an unboxed unlifted data-types, therefore it cannot have any
  -- linear values hidden in a closure anywhere. Therefore it is safe to call
  -- non-linear functions linearly on this type: there is no difference between
  -- copying an 'Int#' and using it several times. /!\
  consume (I# i) = toLinear (\_ -> ()) i


-- | Like 'seq' but since the first argument is restricted to be of type @()@ it
-- is consumed, hence @seqUnit@ is linear in its first argument.
seqUnit :: () #-> b #-> b
seqUnit () b = b

-- | Like 'seq' but the first argument is restricted to be 'Consumable'. Hence the
-- first argument is 'consume'-ed and the result consumed.
lseq :: Consumable a => a #-> b #-> b
lseq a b = seqUnit (consume a) b


-- | Beware: @($)@ is not compatible with the standard one because it is
-- higher-order and we don't have multiplicity polymorphism yet.
($#) :: (a #-> b) #-> a #-> b
-- XXX: Temporary as `($)` should get its typing rule directly from the type
-- inference mechanism.
($#) f x = f x

infixr 0 $#
